<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\NorthernLigtsService;
use App\Services\GoogleSearch;
use DateTime;
use Illuminate\Support\Facades\Input;

class LiveController extends Controller
{   
    private $google;
    private $service;
    public function __construct(NorthernLigtsService $service, GoogleSearch $google){
        $this->northernLigtsService = $service;
        $this->googleSearch = $google;
    }

    public function index(Request $request){
        $this->validate($request, [
            'city' => 'required|string'
        ]);

        $latLong = $this->googleSearch->findLatLongByCity($request->input('city'));
        return response($this->northernLigtsService->index($latLong), 200);
    }

    public function test(){
       
     $handle = fopen("ftp://ftp.swpc.noaa.gov/pub/weekly/27DO.txt", "r");
        $myarray = [];
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                // process the line read.
                $myarray[] = explode(" ", $line);
            }
            fclose($handle);
        }
        $newArray = [];
        foreach($myarray as $key => $item){
            if($key >= 11){
                $string = end($item);
               
                $year = $item[0];
                if(isset($item[1])){
                  $moth = $item[1];
                }else{
                    $moth = 0;
                }
                 if(isset($item[2])){
                  $date = $item[2];
                }else{
                    $date = 0;
                }
                $fulldate = $year . '-' . $moth . '-' . $date;
                $newArray[] = array(
                     "date" => date_format( new DateTime($fulldate), 'd-m-Y'),
                     "kpIndex" => preg_replace('~[\r\n\t]+~', '', $string),
                );
            }
        } 
        return $newArray;
    }
}
