<?php

namespace App\Services;

class GoogleSearch {
    //TODO moved this to constants instead.
    private $key = 'AIzaSyCtB1QV6PcOJa0WPQR6WGH3b_XPs5IXG1Y';
    private $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';

    public function findLatLongByCity($city)
    {
        $result =  $this->place($city);
        $format = json_decode($result);
        $content = array(
            "location" => array(
                "city" => $city,
                "lat" => $format->results[0]->geometry->location->lat,
                "lng" => $format->results[0]->geometry->location->lng
            )
        );
        return $content;
    } 

    private function place($city)
    {
        $ch = curl_init($this->url. $city .'&key=' . $this->key); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch); 
        return $output;
    }
}