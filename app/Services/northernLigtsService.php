<?php

namespace App\Services;
use Illuminate\Http\Request;
use DateTime;

class NorthernLigtsService {
    //TODO moved to constants/config file
    private $url = 'https://api.auroras.live/v1/?type=all&';

    public function index($location){
        $data = $this->liveData($location);
        return $this->filterDataResourceCurrentWeather($data, $location);
    }

    private function liveData($location){
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $this->url .'lat=' . $location['location']['lat'] . '&long=' . $location['location']['lng']); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch); 
        return $output;
    }

    private function forecast($data, $location){
        return $this->getForecastWeather($location);
       $array = [];
       $oldIndex = 0;
       $newIndex = 0;
    
       foreach($data->weather->forecast as $key => $forcast){
           $array[] = array(
                "date" => date_format( new DateTime($forcast->date), 'd-m-Y'),
                "kpIndex" => $this->getKpIndex(date_format( new DateTime($forcast->date), 'd-m-Y')),
                "temperature" => $forcast->temperature,
                "icon" => asset('/img/icons/overskyet_med_sludd.png')
            );
        } 
        return $array;
    }

    private function getForecastWeather($location){
        
        //Getting the kpIndex for the next 27 days 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://noly.app/api/test'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch); 
        
       $forecastIndexArray = json_decode($output, true);
        
        //Now we need to get the wather for the next 7 days. 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://api.darksky.net/forecast/8466df872bf1f4a8621577fa244f9aa3/' .  $location['location']['lat'] . ',' . $location['location']['lng']); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $weather = curl_exec($ch); 
        curl_close($ch); 
        
        $forecastWeather = $weather;
        $json = json_decode($forecastWeather, true);
        
        $weatherArray = [];
        foreach($json['daily']['data'] as $key => $value)
        {
            if($key > 1){
                $weatherArray[] = array(
                    "date" => gmdate('d-m-Y', $value['time']),
                    "icon" =>  asset('/img/icons/overskyet_med_sludd.png'), //$value['icon'],
                    "temperature" => number_format(((int)$value['temperatureMax'] - 32) / 1.8, 2, '.', ''),
                    "kpIndex" =>  $this->mapKpIndexToWeather($forecastIndexArray,  gmdate('d-m-Y', $value['time'])),
                    "status" => "Ukjent"
                );
            }
          
        }

       return $weatherArray;
    }

    private function mapKpIndexToWeather($forecastIndexArray, $date){
        //Mapping the KP index to the date
        foreach($forecastIndexArray as $key => $value){
            if($date === $value['date']){
                return $value['kpIndex'];
            }
        }
    }

    private function filterDataResourceCurrentWeather($data, $location)
    {
        $format = json_decode($data);
        $result = array(
            "date" => $format->date,
            "status" => "Go outside",
            "currentAce" => array(
                "bz" => $format->ace->bz,
                "density" => $format->ace->density,
                "speed" => $format->ace->speed,
                "kpIndex1hour" => $format->ace->kp1hour,
                "kpIndex4hour" => $format->ace->kp4hour,
                "kpIndex" => $format->ace->kp
            ),
            "weather" => array(
                "temperature" => $format->weather->temperature,
                "cloud" => $format->weather->cloud,
                "rain" => $format->weather->rain,
                "fog" => $format->weather->fog,
                "icon" => asset('/img/icons/dag_delvis_skyfritt_regn_hvitt.png'),
                "forecast" => $this->forecast($format, $location)
            ),
            /*"extra" => array(
                "sunrise" => $format->weather->sunrise,
                "sunset" => $format->weather->sunset,
                "moonrise" => $format->weather->moonrise,
                "moonset" => $format->weather->moonset,
                "moonphase" => $format->weather->moonphase
            ),  */
            "location" => $location
        );
        return $result;
    }
    // //"icon" => iconGeneratorService($format->weather->cloud, $format->weather->rain, $format->weather->fog)
}
